<?php
/**
 * @author Dmytro Sydorenko (sidorenko4er@gmail.com)
 * Creation date - 2017-04-06
 * This is a simple php project based on 3 Docker containers:
 * Nginx + PHP (7) + MySQL (5.7)
 * Contains only 1 php file (index.php) which prints phpinfo (branch: 'simple_php_app_1_0')
 * https://i.imgur.com/qUyxmvj.png
 */
phpinfo();