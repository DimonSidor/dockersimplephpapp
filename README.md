"# My project's README" 

 * Author: Dmytro Sydorenko (sidorenko4er@gmail.com)
 * Creation date - 2017-04-06
 * This is a simple php project based on 3 Docker containers:
 * Nginx + PHP (7) + MySQL (5.7)
 * Contains only 1 php file (index.php) which prints phpinfo (branch: 'simple_php_app_1_0')
 * https://i.imgur.com/qUyxmvj.png
 
 * Coming soon:
  PHPUnit Docker Container (https://hub.docker.com/r/phpunit/phpunit/)


 Requirements:
 Docker - https://www.docker.com/
 
 * Run Docker
 
 * Run docker-compose up (on master branch - https://i.imgur.com/sn58vYY.png)
 - my own prepared environment for testing (https://i.imgur.com/3gpaVml.png)
 
 * Run docker-compose ps (to see that you have successfully create and run all three containers)
 
 * Execute www-data/db_dump.sql if needed
 
 * Main page on http://192.168.99.100/ (on Win 7 Docker machine)
 
 * DB on http://192.168.99.100:3306 (db: dev_test, user: testuser, pwd: 0000)
 
 * After finish run docker-compose down (stops containers and removes containers, networks, volumes, and images created by up.)
 
 
 For task 2:
 
 * Checkout to branch sydorenko_test_2
 
 * Execute all from www-data/db_dump.sql using http://192.168.99.100:3306 (db: dev_test, user: testuser, pwd: 0000)
 
 * Run docker-compose up
 
 * Go to 192.168.99.100 (in my case) https://i.imgur.com/aP8LhAG.png https://i.imgur.com/bqxLEFU.png
  and check all functional
 
 * Test passed (https://i.imgur.com/dGC4hjO.png)
 
 * Enjoy - all works :-)
 
 For task 3
 
 * Checkout to branch sydorenko_test_2
 
 * Look at 3_code_review/postgre_task.php